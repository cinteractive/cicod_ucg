package CROWN.CICOD.UCG;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.DatePicker.DatePickerJE;
import static CROWN.utility.ExcelUtil.DoSelectValuesByIndex;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginCorrectDetails;

public class FilterVarianceReport extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginCorrectDetails();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void UCG() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UcgBTN_XPATH", 60);
    }

    @Description("Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void Collection() throws IOException, InterruptedException {
        DoClickWhenReadyJS("CollectionBTN_XPATH", 60);
    }

    @Description("Variance Report")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void VarianceReport() throws IOException, InterruptedException {
        DoClickWhenReadyJS("VarianceReport_XPATH", 60);
    }

    @Description("Date From")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void DateFrom() throws IOException, InterruptedException {
        DatePickerJE("From_XPATH", "06-04-2018");
    }

    @Description("Date To")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void DateTo() throws IOException, InterruptedException {
        DatePickerJE("To_XPATH", "01-31-2021");
    }

    @Description("Month")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void Month() throws IOException, InterruptedException {
        DoSelectValuesByIndex("Against_XPATH", 1, 20);
    }

    @Description("Year")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Year() throws IOException, InterruptedException {
        DoSendKeysWhenReady("Year_XPATH", "Year_TEXT", 20);
    }

    @Description("Submit")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void Submit() throws IOException, InterruptedException {
        getDriver().findElement(By.id("download_button")).click();
    }
}
