package CROWN.CICOD.UCG;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.openqa.selenium.By;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginCorrectDetails;

public class FilterSettlementCollection extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginCorrectDetails();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void UCG() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UcgBTN_XPATH", 60);
    }

    @Description("Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void Collection() throws IOException, InterruptedException {
        DoClickWhenReadyJS("CollectionBTN_XPATH", 60);
    }

    @Description("Settlement Report")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void SettlementReport() throws IOException, InterruptedException {
        DoClickWhenReadyJS("SettlementReport_XPATH", 60);
    }

    @Description("Date From")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void DateFrom() throws IOException, InterruptedException {
        Thread.sleep(4000);
        getDriver().findElement(By.xpath("/html/body/div[5]/div[2]/div/div/div[1]/div/div/div[2]/div/div[1]/ng-datepicker/div[1]/input")).click();
    }

    @Description("Date From")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6, invocationCount = 30)
    public void DateFrom1() throws IOException, InterruptedException {
        getDriver().findElement(By.xpath("//i[2]")).click();
    }

    @Description("Date From")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void DateFrom13() throws IOException, InterruptedException {
        getDriver().findElement(By.xpath("//span[8]/span")).click();
    }

    @Description("Search")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 8)
    public void Search() throws IOException, InterruptedException {
        DoClickWhenReadyJS("D1_XPATH", 10);
    }
/*
    @Description("Assert Search Settlement Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 9)
    public void AssertSearchSettlementCollection() throws IOException, InterruptedException {
        if (getDriver().findElements(By.xpath(Utility.fetchLocator("assertfilter_XPATH"))).size() != 0) {
            test.log(Status.PASS, "Filter By Date Settlement report was successful");
        } else {
            test.log(Status.FAIL, "Filter By Date Settlement report Failed");
        }
    }

 */
}
