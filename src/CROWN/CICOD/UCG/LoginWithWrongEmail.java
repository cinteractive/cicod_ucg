package CROWN.CICOD.UCG;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementNotPresent;
import static CROWN.utility.Login.LoginWrongEmail;

public class LoginWithWrongEmail extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginWrongEmail();
    }

    @Description("Assert Login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void AssertLogin() throws IOException, InterruptedException {
        CheckElementNotPresent("com_XPATH",20);
    }
}
