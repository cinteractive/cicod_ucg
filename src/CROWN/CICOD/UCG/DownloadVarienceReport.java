package CROWN.CICOD.UCG;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginCorrectDetails;
import static CROWN.utility.Utility.DoclickWhenReady;

public class DownloadVarienceReport extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginCorrectDetails();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void UCG() throws IOException, InterruptedException {
        DoclickWhenReady("UcgBTN_XPATH", "UcgBTN_XPATH", 60);
    }

    @Description("Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void Collection() throws IOException, InterruptedException {
        DoClickWhenReadyJS("CollectionBTN_XPATH", 60);
    }

    @Description("All Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void AllCollection() throws IOException, InterruptedException {
        DoClickWhenReadyJS("VarianceReport_XPATH", 60);
    }
}
