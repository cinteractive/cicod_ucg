package CROWN.CICOD.UCG;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.ExcelUtil.DoSelectValuesByIndex;
import static CROWN.utility.ExcelUtil.DoSendKeysWhenReady;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.JavaScriptUtil.DoscrolltoView;
import static CROWN.utility.Login.LoginCorrectDetails;

public class ReprintRecipt extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginCorrectDetails();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void UCG() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UcgBTN_XPATH", 60);
    }

    @Description("Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void Collection() throws IOException, InterruptedException {
        DoClickWhenReadyJS("CollectionBTN_XPATH", 60);
    }

    @Description("All Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void AllCollection() throws IOException, InterruptedException {
        Thread.sleep(3000);
        DoClickWhenReadyJS("AllCollectionBTN_XPATH", 60);
    }

    @Description("All Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void InVoiceNumber() throws IOException, InterruptedException {
        Thread.sleep(3000);
        DoSelectValuesByIndex("UCGmethod_XPATH", 0, 20);
    }

    @Description("Correct TransactionID Search Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 11)
    public void TransactionIDSearchCollection() throws IOException, InterruptedException {
        Thread.sleep(3000);

        DoSelectValuesByIndex("allsear_XPATH", 0, 20);
        DoSendKeysWhenReady("SearchCollectionInputBox_XPATH", "TransactionID_TEXT", 20);
    }

    @Description("Search TransactionID")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 12)
    public void SearchTransactionID() throws IOException, InterruptedException {
        DoscrolltoView("jj_XPATH",20);
        DoClickWhenReadyJS("jj_XPATH", 60);
    }

    @Description("Reprint Receipt")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 13)
    public void ReprintRecipt() throws IOException, InterruptedException {
        Thread.sleep(3000);
      // DoClickWhenReadyJS("ReprintBTN_XPATH", 20);
    }

  }