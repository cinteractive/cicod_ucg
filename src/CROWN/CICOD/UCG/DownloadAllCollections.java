package CROWN.CICOD.UCG;

import CROWN.Base.TestBase;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.springframework.context.annotation.Description;
import org.testng.annotations.Test;

import java.io.IOException;

import static CROWN.utility.Assertion.CheckElementPresent;
import static CROWN.utility.JavaScriptUtil.DoClickWhenReadyJS;
import static CROWN.utility.Login.LoginCorrectDetails;

public class DownloadAllCollections extends TestBase {

    @Description("login")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 1)
    public void login() throws IOException, InterruptedException {
        LoginCorrectDetails();
    }

    @Description("UCG")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 2)
    public void UCG() throws IOException, InterruptedException {
        DoClickWhenReadyJS("UcgBTN_XPATH", 60);
    }

    @Description("Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 3)
    public void Collection() throws IOException, InterruptedException {
        DoClickWhenReadyJS("CollectionBTN_XPATH", 60);
    }

    @Description("All Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 4)
    public void AllCollection() throws IOException, InterruptedException {
        DoClickWhenReadyJS("AllCollectionBTN_XPATH", 60);
    }

    @Description("Download Collection")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 5)
    public void DownloadCollection() throws IOException, InterruptedException {
        DoClickWhenReadyJS("DownloadCollectionBTN_XPATH", 60);
    }

    @Description("Assert Sent Email")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 6)
    public void AssertSentEmail() throws IOException, InterruptedException {
        CheckElementPresent("AssertSentEmail_XPATH",20);
    }

    @Description("Assert Download")
    @Severity(SeverityLevel.NORMAL)
    @Test(priority = 7)
    public void AssertDownload() throws IOException, InterruptedException {
        CheckElementPresent("AssertSentEmail_XPATH",20);
    }
}
