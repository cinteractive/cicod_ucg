package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ActionsClass extends TestBase {

    public static void DoSendKeysWithMoveToElement(String locator, String value, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        Actions action = new Actions(getDriver());
        action.moveToElement(element).sendKeys(Utility.fetchLocator(value)).build().perform();
    }

    public static void DoActionsSendKeys(String locator, String value, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        Actions action = new Actions(getDriver());
        action.sendKeys(element, Utility.fetchLocator(value)).perform();
    }

    public static void DoActionsSendKeysRandomListword(WebDriver driver, String locator, int timeOut) throws IOException, InterruptedException {
        Randomstuff randomstuff = new Randomstuff();
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        Actions action = new Actions(driver);
        action.sendKeys(element, randomstuff.ListRandom()).perform();
    }

    public static void DoSendKeysByActionClassFluentWait(String locator, String value, int timeOut) throws IOException, InterruptedException {
        WebElement locat = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        Actions ac = new Actions(getDriver());
        ac.sendKeys((locat), Utility.fetchLocator(value)).perform();
    }

    public static void DoSendKeysByActionClassWhenReady(String locator, String actualText, String ObjectName, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        Actions ac = new Actions(getDriver());
        ac.sendKeys((locat), Utility.fetchLocator(actualText)).perform();
    }


    //**********************Clicks ********************
    public static void DoActionsClick(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        Actions action = new Actions(getDriver());
        action.click(element).perform();
    }

    public static void DoClickWithMoveToElement(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        Actions action = new Actions(getDriver());
        action.moveToElement(element).click().build().perform();
    }

    public static void DoDoubleClickActionWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element11p = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        element11p.click();
        Actions actionp = new Actions(getDriver());
        actionp.moveToElement(element11p).doubleClick().perform();
    }

    @Test
    public static void DoClickActionclassWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        Actions ac = new Actions(getDriver());
        ac.click(locat).perform();
    }

    public static void DoClickActionclassFluentWait(String locator, String ObjectName, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1100);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        Utility utility = new Utility();
        utility.DoFluentWait(locator, timeOut);
        WebElement locat = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        Actions ac = new Actions(getDriver());
        ac.click(locat).perform();
        System.out.println("waited for " + Utility.fetchLocator(ObjectName) + " to be present on the page -->" + timeOut + " milliseconds");
    }

    public static void DoClearActionclassWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        Actions ac = new Actions(getDriver());
        ac.sendKeys((locat), Keys.DELETE).perform();
    }

    public static void DoHovereffectClickWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        Actions actions = new Actions(getDriver());
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement subMenu = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        actions.moveToElement(subMenu);
        actions.click().build().perform();
    }

    public static void DoPressEnter() {
        try {
            Thread.sleep(5000l);
            Actions ac = new Actions(getDriver());
            ac.sendKeys(Keys.ENTER).perform();
        } catch (InterruptedException e) {

            e.printStackTrace();
        }}
}
