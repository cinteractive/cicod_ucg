package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;

public class DatePicker extends TestBase {

    public static void DatePickerJE(String locator, String Date) throws IOException, InterruptedException {
        WebElement select = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].type = arguments[1]", select, "text");
        select.clear();
        select.sendKeys(Date);
    }

    public static void SetDate(String Dater, String locator, String Day) {
        getDriver().findElement(By.xpath(Dater)).click();
        List<WebElement> list = getDriver().findElements(By.xpath(locator));

        for(WebElement e : list) {
            if(e.getText().equals(Day)) {
                e.click();
                break;
            }
        }
    }

}