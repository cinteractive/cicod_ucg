package CROWN.utility;

import CROWN.Base.TestBase;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Randomstuff extends TestBase {

    private static final Random rand = new Random();

    public static String ListRandom() {

        String fileName = "./Config/" +"words.txt";
        File wordList = new File(fileName);
        List<String> words = new ArrayList<>();
        Scanner reader = null;

        try {
            reader = new Scanner(wordList);
        } catch (FileNotFoundException e) {
            System.out.println("file \"" + fileName + "\" not found");
            System.exit(0);
        }

        while (reader.hasNextLine()) {
            String word = reader.nextLine();
            words.add(word);
        }

        int wordNum = words.size();
        int place;
        place = rand.nextInt(wordNum);
        String s = words.get(place);
        return s;
    }

    public static String RandomEmail() {
        String a = "@gmail.com";
        return (ListRandom() + a);
    }
}
