package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import java.io.IOException;
import static CROWN.utility.ActionsClass.DoSendKeysByActionClassWhenReady;
import static CROWN.utility.Utility.DoclickWhenReady;

public class Login extends TestBase {

    public static void AcceptAlert() throws IOException, InterruptedException {
        if (getDriver().switchTo().alert() != null) {
            Alert alert = getDriver().switchTo().alert();
            alert.accept();
        }
    }

    public static void AlertDismis() throws IOException, InterruptedException {
        if (getDriver().switchTo().alert() != null) {
            Alert alert = getDriver().switchTo().alert();
            alert.dismiss();
        }
    }

    public static void LoginDef1() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "DefShopName_TEXT", "DefShopName_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "DefEmail_TEXT", "DefEmail_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "DefPassword_TEXT", "DefPassword_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginNexus() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "NexusDomainName1_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "NexusEmail1_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "NexusPassword1_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginPremium() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "PshopName_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "Pemail_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "Ppassword_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginWrongDomainName() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "DefaultShopName_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "UcgEmail1_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "UcgPassword1_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginWrongEmail() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "UcgDomainName1_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "DefaultEmail_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "UcgPassword1_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginWrongPassWord() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "UcgDomainName1_TEXT", "UcgDomainName1_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "UcgEmail1_TEXT", "UcgEmail1_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "DefaultPassWord_TEXT", "UcgPassword1_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginCorrectDetails() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "UcgDomainName1_TEXT", "UcgDomainName1_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "UcgEmail1_TEXT", "UcgEmail1_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "UcgPassword1_TEXT", "UcgPassword1_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginDefault() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "DefaultShop_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "Defem_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "defpass_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void Loginlupin() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "lupinDomainName_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "lupinEmail_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "lupinPass_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void Login1() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "NexusDomainName1_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "NexusEmail1_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "NexusPassword1_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void Login() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "NexusDomainName1_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "NexusEmail1_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "NexusPassword1_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginExpiredAccount() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "Cshopname_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "Cemail_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "Cpassword_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginUpgrade() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "TestShopName_XPATH", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "TestEmail_XPATH", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "UpPassword_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginTestAccount() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "Shopname_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "Email_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "Password_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }

    public static void LoginTestAccountSetUp() throws IOException, InterruptedException {
        DoSendKeysByActionClassWhenReady("DomainName_XPATH", "Ashop_TEXT", "DOma_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Email_XPATH", "Aemail_TEXT", "Emailf_TEXT", 20);
        DoSendKeysByActionClassWhenReady("Password_XPATH", "Apassword_TEXT", "Passw_TEXT", 20);
        DoclickWhenReady("LoginBTN_XPATH", "Logi_TEXT", 30);
    }


    public static void LoginMultipleAccountEKEDC() throws IOException, InterruptedException {
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(Utility.fetchLocator("Loginbtn_XPATH"))).click();
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lemail_XPATH"))).sendKeys(Utility.fetchLocator("MultiEMail_TEXT"));
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lpassword_XPATH"))).sendKeys(Utility.fetchLocator("MultiPassword_TEXT"));

        Thread.sleep(2000);
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator("Lcheckbox_XPATH")));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);

        Thread.sleep(2000);
        getDriver().findElement(By.xpath(Utility.fetchLocator("ActualRegisterBTN_XPATH"))).click();
    }

    public static void LoginEKEDC() throws IOException, InterruptedException {

        getDriver().findElement(By.xpath(Utility.fetchLocator("Loginbtn_XPATH"))).click();
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lemail_XPATH"))).sendKeys(Utility.fetchLocator("ExistEmail_TEXT"));
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lpassword_XPATH"))).sendKeys(Utility.fetchLocator("password_TEXT"));

        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator("Lcheckbox_XPATH")));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);

        getDriver().findElement(By.xpath(Utility.fetchLocator("ActualRegisterBTN_XPATH"))).click();
    }


    public static void Login_with_RegEmailEKEDC() throws IOException {

        getDriver().findElement(By.xpath(Utility.fetchLocator("Loginbtn_XPATH"))).click();
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lemail_XPATH"))).sendKeys(Utility.fetchLocator("ExistEmail_TEXT"));
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lpassword_XPATH"))).sendKeys(Utility.fetchLocator("password_TEXT"));

        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator("Lcheckbox_XPATH")));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);

        getDriver().findElement(By.xpath(Utility.fetchLocator("ActualRegisterBTN_XPATH"))).click();

    }

    public static void Login_with_Wrong_EmailEKEDC() throws IOException {

        getDriver().findElement(By.xpath(Utility.fetchLocator("Loginbtn_XPATH"))).click();
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lemail_XPATH"))).sendKeys(Utility.fetchLocator("WrongEmail_TEXT"));
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lpassword_XPATH"))).sendKeys(Utility.fetchLocator("RegNumber_TEXT"));

        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator("Lcheckbox_XPATH")));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);

        getDriver().findElement(By.xpath(Utility.fetchLocator("ActualRegisterBTN_XPATH"))).click();

    }

    public static void Login_with_Wrong_PasswordEKEDC() throws IOException {

        getDriver().findElement(By.xpath(Utility.fetchLocator("Loginbtn_XPATH"))).click();
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lemail_XPATH"))).sendKeys(Utility.fetchLocator("WrongEmail_TEXT"));
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lpassword_XPATH"))).sendKeys(Utility.fetchLocator("WrongPassword_TEXT"));

        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator("Lcheckbox_XPATH")));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);

        getDriver().findElement(By.xpath(Utility.fetchLocator("ActualRegisterBTN_XPATH"))).click();
    }


    public static void Login_PostPaidLandlordEKEDC() throws IOException {

        getDriver().findElement(By.xpath(Utility.fetchLocator("Loginbtn_XPATH"))).click();
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lemail_XPATH"))).sendKeys(Utility.fetchLocator("EEmail_TEXT"));
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lpassword_XPATH"))).sendKeys(Utility.fetchLocator("EPassword_TEXT"));

        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator("Lcheckbox_XPATH")));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);

        getDriver().findElement(By.xpath(Utility.fetchLocator("ActualRegisterBTN_XPATH"))).click();
    }

    public static void LoginPrePaidEKEDC() throws IOException {

        getDriver().findElement(By.xpath(Utility.fetchLocator("Loginbtn_XPATH"))).click();
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lemail_XPATH"))).sendKeys(Utility.fetchLocator("PreEmail_TEXT"));
        getDriver().findElement(By.xpath(Utility.fetchLocator("Lpassword_XPATH"))).sendKeys(Utility.fetchLocator("PrePassword_TEXT"));

        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator("Lcheckbox_XPATH")));
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);

        getDriver().findElement(By.xpath(Utility.fetchLocator("ActualRegisterBTN_XPATH"))).click();
    }
}
