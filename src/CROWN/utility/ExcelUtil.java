package CROWN.utility;

import CROWN.Base.TestBase;
import com.github.javafaker.Faker;
import com.mifmif.common.regex.Generex;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;

import java.io.IOException;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static CROWN.utility.Randomstuff.ListRandom;

public class ExcelUtil extends TestBase {

    public static void CheckBusinessNameExist() throws IOException, InterruptedException {
        if (getDriver().findElements(By.xpath(Utility.fetchLocator("usenamechecker_XPATH"))).size() != 0) {
            Faker faker = new Faker();
            String BusinessName = faker.name().firstName() + faker.name().lastName();

            System.out.println("New Business Name : " + BusinessName);
            DoSendkeyFluentWait("Businessname_XPATH", BusinessName, 2, 20);
            DoClickFluentWait("NextButton_XPATH", 2, 20);
        } else {
            System.out.println("Business Name Doesn't Exist");
        }
    }

    public static void DoSendkeyFluentWait(final String locator, String Value, int Retry, int timeOut) throws IOException, InterruptedException {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofSeconds(Retry))
                .ignoring(NoSuchElementException.class);

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                try {
                    return getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        element.clear();
        element.sendKeys(Value);
    }

    public static void DoClickFluentWait(final String locator, int Retry, int timeOut) throws IOException, InterruptedException {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofSeconds(Retry))
                .ignoring(NoSuchElementException.class);

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                try {
                    return getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        element.click();
    }


    public static void DoSendkeyFluentWaitUtility(final String locator, String value, int Retry, int timeOut) throws IOException, InterruptedException {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofSeconds(Retry))
                .ignoring(NoSuchElementException.class);

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                try {
                    return getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        element.clear();
        element.sendKeys(Utility.fetchLocator(value));
    }

    public static String Password(int minimumLength, int maximumLength, boolean includeUppercase, boolean includeSpecial) {
        Faker faker = new Faker();
        if (includeSpecial) {
            char[] password = faker.lorem().characters(minimumLength, maximumLength, includeUppercase).toCharArray();
            char[] special = new char[]{'!', '@', '#', '$', '%', '^', '&', '*'};
            for (int i = 0; i < faker.random().nextInt(minimumLength); i++) {
                password[faker.random().nextInt(password.length)] = special[faker.random().nextInt(special.length)];
            }
            return new String(password);
        } else {
            return faker.lorem().characters(minimumLength, maximumLength, includeUppercase);
        }
    }

    public void isAlertPresent() throws InterruptedException {
        Thread.sleep(1500);
        try {
            getDriver().switchTo().alert().accept();
            test.get().pass("Alert Is Present During This Test");
        } catch (NoAlertPresentException e) {
            System.out.println("Alert is not Present");
            test.get().fail("Alert Not Present During this Test");
        }
    }

    public static void DoSendKeysWhenReadyPhoneNumber(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        String regex1 = "080(1|3|7|8)\\d{7}";
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        element.clear();
        element.sendKeys(new Generex(regex1).random());
    }

    public static void DoSendKeysWhenReadyLinkText(String locator, String actualText, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.linkText(Utility.fetchLocator(locator)))));
        element.clear();
        element.sendKeys(Utility.fetchLocator(actualText));
    }

    public static void DoSendKeysWhenReadyName(String locator, String actualText, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.name(Utility.fetchLocator(locator)))));
        element.clear();
        element.sendKeys(Utility.fetchLocator(actualText));
    }

    public static void DoSendKeysWhenReadyID(String locator, String actualText, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.id(Utility.fetchLocator(locator)))));
        element.clear();
        element.sendKeys(Utility.fetchLocator(actualText));
    }

    public static void DosendKeysRandomNumberWhenReadyLinkText(String locator, int span, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        SecureRandom rn = new SecureRandom();
        int resourcetype = rn.nextInt(span) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.linkText(Utility.fetchLocator(locator)))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + resourcetype);
    }

    public static void DosendKeysRandomListwordsWhenReadyXpath(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + ListRandom());
    }
    public static void DosendKeysRandomListwordsWhenReadyLinkText(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(Utility.fetchLocator(locator))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + ListRandom());
    }

    public static void DosendKeysRandomNumberWhenReadyID(String locator, int span, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        SecureRandom rn = new SecureRandom();
        int resourcetype = rn.nextInt(span) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.id(Utility.fetchLocator(locator)))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + resourcetype);
    }

    public static void DosendKeysRandomListwordsWhenReadyID(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id(Utility.fetchLocator(locator))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + ListRandom());
    }

    public static void DosendKeysRandomNumberWhenReadyName(String locator, int span, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        SecureRandom rn = new SecureRandom();
        int resourcetype = rn.nextInt(span) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.name(Utility.fetchLocator(locator)))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + resourcetype);
    }

    public static void DosendKeysRandomListwordsWhenName(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.name(Utility.fetchLocator(locator))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + ListRandom());
    }

    public static void doclickWhenReadyID(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.id(Utility.fetchLocator(locator)))));
        element.click();
    }

    public static void doclickWhenReadyLinkText(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.linkText(Utility.fetchLocator(locator)))));
        element.click();
    }

    public static void doclickWhenReadyName(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.name(Utility.fetchLocator(locator)))));
        element.click();
    }

    public static void DoSelectValuesByVisibleTextLinkText(String locator, String value, int timeOut) throws IOException, InterruptedException {
        WebElement locat = getDriver().findElement(By.linkText(Utility.fetchLocator(locator)));
        locat.click();
        Select select = new Select(locat);
        select.selectByVisibleText(Utility.fetchLocator(value));
    }

    public static void DoSelectValuesByIndexLinkText(String locator, int index, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.linkText(Utility.fetchLocator(locator)))));
        locat.click();
        Select select = new Select(locat);
        select.selectByIndex(index);
    }

    public static void DoSelectValuesByIndexRandomID(String locator, int Range, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        SecureRandom rn = new SecureRandom();
        int stt = rn.nextInt(Range) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.id(Utility.fetchLocator(locator)))));        Select select = new Select(locat);
        locat.click();
        select.selectByIndex(stt);
    }

    public static void DoSelectValuesByIndexRandomLinkText(String locator, int Range, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        SecureRandom rn = new SecureRandom();
        int stt = rn.nextInt(Range) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.linkText(Utility.fetchLocator(locator)))));        Select select = new Select(locat);
        locat.click();
        select.selectByIndex(stt);
    }

    public static void DoSelectValuesByValueLinkText(String locator, String value, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.linkText(Utility.fetchLocator(locator)))));
        locat.click();
        Select select = new Select(locat);
        select.selectByValue(Utility.fetchLocator(value));
    }



    public static void DoSelectValuesByVisibleTextName(String locator, String value, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebElement locat = getDriver().findElement(By.name(Utility.fetchLocator(locator)));
        locat.click();
        Select select = new Select(locat);
        select.selectByVisibleText(Utility.fetchLocator(value));
    }

    public static void DoSelectValuesByIndexName(String locator, int index, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.name(Utility.fetchLocator(locator)))));
        locat.click();
        Select select = new Select(locat);
        select.selectByIndex(index);
    }

    public static void DoSelectValuesByIndexID(String locator, int index, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.id(Utility.fetchLocator(locator)))));
        locat.click();
        Select select = new Select(locat);
        select.selectByIndex(index);
    }

    public static void DoSelectValuesByIndexRandomName(String locator, int Range, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        SecureRandom rn = new SecureRandom();
        int stt = rn.nextInt(Range) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.name(Utility.fetchLocator(locator)))));        Select select = new Select(locat);
        select.selectByIndex(stt);
    }

    public static void DoSelectValuesByValueName(String locator, String value, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.name(Utility.fetchLocator(locator)))));
        Select select = new Select(locat);
        select.selectByValue(Utility.fetchLocator(value));
    }

    //**********************Send Keys ********************

    public static void DosendKeysRandomNumberFluentWait(String locator, int span, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        SecureRandom rn = new SecureRandom();
        int resourcetype = rn.nextInt(span) + 1;
        DowaitForElementWithFluentWait(locator, timeOut);
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        element.clear();
        String a = "";
        element.sendKeys(a + resourcetype);
    }

    public static void DosendKeysRandomEmailsFluentWait(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        DowaitForElementWithFluentWait(locator, timeOut);
        Randomstuff randomstuff = new Randomstuff();
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        element.clear();
        String a = "@gmail.com";
        element.sendKeys(ListRandom() + a);
    }

    public static void DosendKeyRRFluentWait(String locator, String actualText, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        DowaitForElementWithFluentWait(locator, timeOut);
        WebElement mcj = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        mcj.clear();
        mcj.sendKeys(actualText);
    }

    public static void DoSendKeysFluentWait(String locator, String actualText, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        DowaitForElementWithFluentWait(locator, timeOut);
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        element.clear();
        element.sendKeys(Utility.fetchLocator(actualText));
    }

    public static void DosendKeysRandomListwordsFluentWait(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        Randomstuff randomstuff = new Randomstuff();
        DowaitForElementWithFluentWait(locator, timeOut);
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        element.clear();
        String a = "";
        element.sendKeys(a + ListRandom());
    }

    public static void DosendKeyRRWhenReady(String locator, String actualText, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement mcj = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        mcj.clear();
        mcj.sendKeys(actualText);
    }

    public static void DoSendKeysWhenReady(String locator, String actualText, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        element.clear();
        element.sendKeys(Utility.fetchLocator(actualText));
    }

    public static void DoSendKeysWhenReadyEnter(String locator, String actualText, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        element.clear();
        element.sendKeys(Utility.fetchLocator(actualText) + Keys.ENTER + Keys.ENTER + Keys.ENTER );
    }

    public static void DosendKeysRandomEmailsWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        Randomstuff randomstuff = new Randomstuff();
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        element.clear();
        String a = "@gmail.com";
        element.sendKeys(ListRandom() + a );
    }

    public static void DosendKeysRandomNumberWhenReady(String locator, int span, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        SecureRandom rn = new SecureRandom();
        int resourcetype = rn.nextInt(span) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + resourcetype);
    }

    public static void DosendKeysRandomListwordsWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        element.click();
        element.clear();
        String a = "";
        element.sendKeys(a + ListRandom());
    }

    //**********************Clicks ********************
    public static void DoscrolltoViewClickFluentWait(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        DowaitForElementWithFluentWait(locator, timeOut);
        WebElement ti11 = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].scrollIntoView();", ti11);
        ti11.click();
    }

    public static void DoclickFluentWait(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        DowaitForElementWithFluentWait(locator, timeOut);
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        element.click();
    }

    public static void doclickWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        element.click();
    }

    public static void DoscrolltoViewClickWhenReady(String locator, int timeOut) throws IOException, InterruptedException {
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement ti11 = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        jse.executeScript("arguments[0].scrollIntoView();", ti11);
        ti11.click();
    }

    //********************drop down utils **************************
    public static void DoSelectValuesByVisibleText(String locator, String value, int timeOut) throws IOException, InterruptedException {
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        WebElement locat = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        locat.click();
        Select select = new Select(locat);
        select.selectByVisibleText(Utility.fetchLocator(value));
    }

    public static void DoSelectValuesByIndex(String locator, int index, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        locat.click();
        Select select = new Select(locat);
        select.selectByIndex(index);
    }

    public static void DoSelectValuesByIndexRandom(String locator, int Range, int timeOut) throws IOException, InterruptedException {
        SecureRandom rn = new SecureRandom();
        int stt = rn.nextInt(Range) + 1;
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        locat.click();
        Select select = new Select(locat);
        select.selectByIndex(stt);
    }

    public static void DoSelectValuesByValue(String locator, String value, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement locat = wait.until(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
        locat.click();
        Select select = new Select(locat);
        select.selectByValue(Utility.fetchLocator(value));
    }

    public static String DoGetPageCurrentUrl(int timeOut, String urlValue) throws IOException, InterruptedException {
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        wait.until(ExpectedConditions.urlContains(Utility.fetchLocator(urlValue)));
        return getDriver().getCurrentUrl();
    }

    //***************************wait utils ******************************
    public static WebElement DowaitForElementPresent(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        return wait.until(ExpectedConditions.presenceOfElementLocated((By) getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
    }

    public static WebElement DowaitForElementToBeVisible(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    //******************* FluentWait Utils ***********************
    public static WebElement DowaitForElementWithFluentWaitConcept(String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofSeconds(3))
                .ignoring(NoSuchElementException.class);

        return wait.until(ExpectedConditions.presenceOfElementLocated((By) getDriver().findElement(By.xpath(Utility.fetchLocator(locator)))));
    }

    public static WebElement DowaitForElementWithFluentWait(final String locator, int timeOut) throws IOException, InterruptedException {
        Thread.sleep(1200);
        getDriver().manage().timeouts().implicitlyWait(Integer.parseInt((String) Utility.fetchProperty("implicit.wait")), TimeUnit.SECONDS);
        Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
                .withTimeout(Duration.ofSeconds(timeOut))
                .pollingEvery(Duration.ofSeconds(3))
                .ignoring(NoSuchElementException.class);

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                try {
                    return driver.findElement(By.xpath(Utility.fetchLocator(locator)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });

        return element;
    }

}
