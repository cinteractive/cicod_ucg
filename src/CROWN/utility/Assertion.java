package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.IOException;

import static org.testng.AssertJUnit.assertEquals;

public class Assertion extends TestBase {


    public static void DoCheckBoxSelected(String locator, String DisplayPassmsg, String DisplayFailmsg, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement check_box1 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        if (check_box1.isSelected()) {
            test.get().pass(Utility.fetchLocator(DisplayPassmsg));
        } else {
            check_box1.click();
            test.get().fail(Utility.fetchLocator(DisplayPassmsg));
        }
    }

    public static void DoAssertContainsWhenReady(String locator, String Containstext, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        WebElement msg11 = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
        System.out.println(msg11);
        String text11 = msg11.getText();
        Assert.assertTrue(msg11.isEnabled() && text11.contains(Utility.fetchLocator(Containstext)));

    }

    public static void CheckElementPresent(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
    }

    public static void CheckElementNotPresent(String locator, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(Utility.fetchLocator(locator))));
    }

    public static void DoAssertEqualWhenReady(String locator, String assertionString, String DisplayPassmsg, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        assertEquals(Utility.fetchLocator(assertionString), wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Utility.fetchLocator(locator)))).getText());
        test.get().pass(Utility.fetchLocator(DisplayPassmsg));
    }

    public static void DoAssertTittle(String TittleText, int timeOut) throws IOException, InterruptedException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOut);
        wait.until(ExpectedConditions.titleContains(Utility.fetchLocator(TittleText)));
    }
}
