package CROWN.utility;

import CROWN.Base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Downloader extends TestBase {

    public static void ConfirmDownloadLink(String locator) throws IOException, InterruptedException {
        WebElement element = getDriver().findElement(By.xpath(Utility.fetchLocator(locator)));
        String url = element.getAttribute("href");
        verifyLink(url);
    }

    public static void verifyLink(String urlLink) throws IOException, InterruptedException {

        URL link = new URL(urlLink);
        HttpURLConnection httpConn = (HttpURLConnection) link.openConnection();
        httpConn.setConnectTimeout(2000);
        httpConn.connect();

        if (httpConn.getResponseCode() == 200) {
            test.get().pass(urlLink + ":::: is a Valid link ::::" + httpConn.getResponseMessage());
            System.out.println(urlLink + ":::: is a Valid link ::::" + httpConn.getResponseMessage());
        } else {
            test.get().fail(urlLink + ":::: Link is not valid ::::" + httpConn.getResponseMessage());
        }
        TimeUnit.SECONDS.sleep(1);
        System.out.println(httpConn.getResponseCode());


    }
}
